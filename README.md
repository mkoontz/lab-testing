# Lab Testing

This repository is an effort to automatically configure/deploy Telegraf agents to Fedora 35 minimal servers. The base telegraf agent config will allow for DNS, HTTP, and ICMP monitoring of various services. 

## Purpose

This should be an extremely quick way to get various services spun up in a lab environment for network testing. Will allow for the tracking of outages/response failures when working through proof-of-concept evaluations or staging production changes and evaluating the expected customer impact. 

## Variable Configuration

All variable configuration is stored in the vars.yml file. The variables are used when generating the telegraf configuration file for the telegraf agents. 

```
influx_server: 10.1.1.1
influx_database: testdb
influx_user: test
influx_password: changeme
web_address: http://10.1.1.2
dns_server: 10.1.1.3
dns_query: test.lab.int
interval: 10
svr_monitor: yes
```

* influx_server is the IP address where Influx has been installed. 
* influx_database is the name of the database where you want the data stored.
* influx_user user created on the influxdb server for writing to the database.
* influx_password is the password used for logging into the influxdb server
* web_address is the IP or hostname of the web server that is setup for handling Web Requests
* dns_server is the IP or hostname of the DNS server for accepting DNS queries. 
* dns_query is the domain name that is queried by the telegraf agents. 
* interval is the amount of time in seconds that data is sent from telegraf to the influxdb instance
* svr_monitor if this variable is 'yes,' the telegraf configuration also collects MEM/CPU information from the telegraf agent servers and sends it to Influx. 

## Templates

There are a pair of template files stored in the templates directory for configuring telegraf. The telegraf-conf.j2 file becomes the main /etc/telegraf/telegraf.conf file. The override.j2 file is used for the equivalent of systemctl edit to generate an override unit file for allowing telegraf the ability to send icmp probes. 